package com.social.lunetin;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.social.lunetin.Activities.BaseActivity;
import com.social.lunetin.Activities.ChatActivity;
import com.social.lunetin.Activities.GrabUserActivity;
import com.social.lunetin.Activities.LoginActivity;
import com.social.lunetin.Adapters.ChatListAdapter;
import com.social.lunetin.Adapters.ReportAdapter;
import com.social.lunetin.Utils.BroadcastService;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.util;
import com.social.lunetin.network.Apis;
import com.social.lunetin.network.response.ProfileModel;
import com.social.lunetin.network.response.ReportModel;
import com.social.lunetin.network.response.UserListModel;
import com.social.lunetin.network.response.UsernameModel;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvChatList;
    RelativeLayout layoutUsers;
    TextView tvUserName;
    RelativeLayout layoutNoUser;
    String name = "", id = "";
    SwipeRefreshLayout swiperefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        util.setStatusBarGradiant(this);
        if (getIntent().hasExtra("name")) {
            name = getIntent().getStringExtra("name");
        }
        if (getIntent().hasExtra("id")) {
            id = getIntent().getStringExtra("id");
            Log.e("id",id);
        }

        startService(new Intent(this, BroadcastService.class));
        Log.i("TAG", "Started service");
        if (id.equals("")) {
            getProfile();
        }

        rvChatList = findViewById(R.id.rvChatList);
        layoutUsers = findViewById(R.id.layoutUsers);
        tvUserName = findViewById(R.id.tvUserName);
        layoutNoUser = findViewById(R.id.layoutNoUser);
        swiperefresh = findViewById(R.id.swiperefresh);
        rvChatList.setHasFixedSize(true);
        rvChatList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));


        layoutUsers.setOnClickListener(view -> {
            startActivity(new Intent(this, GrabUserActivity.class));
//            this.overridePendingTransition(R.anim.anim_slide_in_left,
//                    R.anim.anim_slide_out_left);
        });

        findViewById(R.id.tvReportaproblem).setOnClickListener(view -> showReportDialog());
        findViewById(R.id.tvFeedback).setOnClickListener(view -> showFeedbackDialog());
        findViewById(R.id.tvRate).setOnClickListener(view -> {
            Uri uri = Uri.parse("http://instagram.com/_u/lunetin_app");
            Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
            likeIng.setPackage("com.instagram.android");
            try {
                startActivity(likeIng);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.instagram.com/lunetin_app/")));
            }
        });

        util.setOnlineStatus(this);
        if (name.equals("")) {
            getProfile();
        } else {
            String upperString = name.substring(0, 1).toUpperCase()
                    + name.substring(1).toLowerCase();
            tvUserName.setText(upperString);
            Map<String, String> map = new HashMap<>();
            if(Utility.getUserName().equals("")){
                map.put("name", tvUserName.getText().toString().trim());
            }
            else  map.put("name", Utility.getUserName());

            map.put("page", "1");
            getUserList(map, true);
        }

        swiperefresh.setOnRefreshListener(() -> {
            Log.e("name",name);
            Log.e("id",id);
            if (name.equals("")) {
                getProfile();
            } else {
                String upperString = name.substring(0, 1).toUpperCase()
                        + name.substring(1).toLowerCase();
                tvUserName.setText(upperString);
                Map<String, String> map = new HashMap<>();
                if(Utility.getUserName().equals("")){
                    map.put("name", tvUserName.getText().toString().trim());
                }
                else  map.put("name", Utility.getUserName());
                map.put("page", "1");
                getUserList(map, true);

            }
        });
    }

    private void showReportDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.report_problem_dialog);
        ImageView imgCancel = dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());
        RecyclerView rvReportList = dialog.findViewById(R.id.rvReportList);
        rvReportList.setHasFixedSize(true);
        rvReportList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        getReportList(rvReportList, dialog);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showReportBriefDialog(String id) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.report_brief_dialog);
        ImageView imgCancel = dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());
        EditText etFeedback = dialog.findViewById(R.id.etFeedback);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);

        tvConfirm.setOnClickListener(view -> {
            dialog.dismiss();
            String text = etFeedback.getText().toString();
            Map<String, String> map = new HashMap<>();
            map.put("reportId", id);
            map.put("text", text);
            submitReport(map);
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showFeedbackDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.feedback_dialog);
        ImageView imgCancel = dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());
        EditText etFeedback = dialog.findViewById(R.id.etFeedback);

        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setOnClickListener(view -> {
            String Feedback = etFeedback.getText().toString().trim();
            if (TextUtils.isEmpty(Feedback)) {
//            Toast.makeText(this, "First Name", Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "Please enter your feedback", Toast.LENGTH_SHORT).show();
                return;
            }
            Map<String, String> map = new HashMap<>();
            map.put("feedback", Feedback);
            submitFeedback(map);
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void getReportList(RecyclerView recyclerView, Dialog dialog) {
        Call<ReportModel> call = Apis.getAPIService().getReportList();
        call.enqueue(new Callback<ReportModel>() {
            @Override
            public void onResponse(@NotNull Call<ReportModel> call, @NotNull Response<ReportModel> response) {
//                dialog.dismiss();
                ReportModel user = response.body();
                Log.e("response", user.toString());
                if (user.getStatusCode().equals("200")) {
                    ReportAdapter reportAdapter = new ReportAdapter(MainActivity.this, user.getData(), pos -> {
                        dialog.dismiss();
                        showReportBriefDialog(user.getData().get(pos).getId());
                    });
                    recyclerView.setAdapter(reportAdapter);
                } else {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ReportModel> call, @NotNull Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });

    }

    public void submitReport(final Map<String, String> map) {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<UsernameModel> call = Apis.getAPIService().submitReport("Bearer " + Utility.getToken(), map);
        call.enqueue(new Callback<UsernameModel>() {
            @Override
            public void onResponse(@NotNull Call<UsernameModel> call, @NotNull Response<UsernameModel> response) {
                dialog.dismiss();
                UsernameModel user = response.body();
                Log.e("response", user.toString());
                if (user.getStatusCode().equals("200")) {
                    Toast.makeText(getApplicationContext(), "Thank you.", Toast.LENGTH_SHORT).show();
                } else if (user.getStatusCode().equals("400")) {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<UsernameModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void submitFeedback(final Map<String, String> map) {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<UsernameModel> call = Apis.getAPIService().submitFeedback("Bearer " + Utility.getToken(), map);
        call.enqueue(new Callback<UsernameModel>() {
            @Override
            public void onResponse(@NotNull Call<UsernameModel> call, @NotNull Response<UsernameModel> response) {
                dialog.dismiss();
                UsernameModel user = response.body();
                Log.e("response", user.toString());
                if (user.getStatusCode().equals("200")) {
                    Toast.makeText(getApplicationContext(), "Thank you.", Toast.LENGTH_SHORT).show();
                } else if (user.getStatusCode().equals("400")) {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<UsernameModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void getProfile() {

        Call<ProfileModel> call = Apis.getAPIService().getProfile("Bearer " + Utility.getToken());
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(@NotNull Call<ProfileModel> call, @NotNull Response<ProfileModel> response) {

                ProfileModel user = response.body();
                Log.e("response", user.toString());
                if (user.getStatusCode().equals("200")) {
                    if (user.getData().getUserdetail().getUsernamelist().size() > 0) {
                        String upperString;
                        if(Utility.getUserName().equals("")){
                             upperString = user.getData().getUserdetail().getUsernamelist().get(0).getUsername().substring(0, 1).toUpperCase()
                                    + user.getData().getUserdetail().getUsernamelist().get(0).getUsername().substring(1).toLowerCase();
                        }
                        else {
                             upperString = Utility.getUserName().substring(0, 1).toUpperCase()
                                    + Utility.getUserName().substring(1).toLowerCase();
                        }

                        tvUserName.setText(upperString);

                        id = user.getData().getUserdetail().getUsernamelist().get(0).getId();
                    }
                    Map<String, String> map = new HashMap<>();
                    if(Utility.getUserName().equals("")){
                        map.put("name", tvUserName.getText().toString().trim());
                    }
                    else  map.put("name", Utility.getUserName());
                    map.put("page", "1");
                    getUserList(map, true);
                } else if (user.getStatusCode().equals("400")) {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProfileModel> call, @NotNull Throwable t) {

                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void getUserList(final Map<String, String> map, boolean isshow) {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if (isshow) {
            dialog.show();
        }

        Call<UserListModel> call = Apis.getAPIService().getUserList("Bearer " + Utility.getToken(), map);
        call.enqueue(new Callback<UserListModel>() {
            @Override
            public void onResponse(@NotNull Call<UserListModel> call, @NotNull Response<UserListModel> response) {
                dialog.dismiss();
                swiperefresh.setRefreshing(false);
                UserListModel user = response.body();
                Log.e("response", user.toString());
                switch (user.getStatusCode()) {
                    case "200":

                        if (user.getData().getTotalrecord().equals("0")) {
                            rvChatList.setVisibility(View.GONE);
                            layoutNoUser.setVisibility(View.VISIBLE);
                        } else {
                            rvChatList.setVisibility(View.VISIBLE);
                            layoutNoUser.setVisibility(View.GONE);
                            ChatListAdapter chatListAdapter = new ChatListAdapter(MainActivity.this, user.getData().getUserlist(), id, pos -> {

                                    startActivity(new Intent(MainActivity.this, ChatActivity.class)
                                            .putExtra("firebaseUserId", user.getData().getUserlist().get(pos).getFirebaseUserId())
                                            .putExtra("hexCodeTop", user.getData().getUserlist().get(pos).getHexCodeTop())
                                            .putExtra("senderId", user.getData().getUserlist().get(pos).getId() )
                                            .putExtra("id",user.getData().getUserlist().get(pos).getId())
                                            .putExtra("user_id", user.getData().getUserlist().get(pos).getUsername_user_id())
                                            .putExtra("blockstatus", user.getData().getUserlist().get(pos).getBlockstatus())
                                            .putExtra("blockby", user.getData().getUserlist().get(pos).getBlockby())
                                            .putExtra("tag","1")
                                            .putExtra("username", user.getData().getUserlist().get(pos).getUsername()));
                                    overridePendingTransition(R.anim.anim_slide_in_left,
                                            R.anim.anim_slide_out_left);


                            });
                            rvChatList.setAdapter(chatListAdapter);
                        }
                        break;
                    case "400":
                        Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                        break;
                    case "409":
                        Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                        Utility.setlogout();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        break;
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserListModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private final BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGUI(intent);
            // or whatever method used to update your GUI fields
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        util.setOnlineStatus(MainActivity.this);
        registerReceiver(br, new IntentFilter(BroadcastService.COUNTDOWN_BR));
        Log.i("TAG", "Registered broacast receiver");
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(br);
//        util.setOfflineStatus(MainActivity.this);
        Log.i("TAG", "Unregistered broacast receiver");
    }

    @Override
    public void onStop() {
        try {
            unregisterReceiver(br);
        } catch (Exception e) {
            // Receiver was probably already stopped in onPause()
        }
//        util.setOfflineStatus(MainActivity.this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, BroadcastService.class));
        Log.i("TAG", "Stopped service");
        util.setOfflineStatus(MainActivity.this);
        super.onDestroy();
    }

    private void updateGUI(Intent intent) {
        if (intent.getExtras() != null) {
            long millisUntilFinished = intent.getLongExtra("countdown", 0);
            Log.i("TAG", "Countdown seconds remaining: " + millisUntilFinished / 1000);
        }
    }

//    private void showBlockDialog() {
//        Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setContentView(R.layout.block_layout);
//        ImageView imgCancel = dialog.findViewById(R.id.imgCancel);
//        imgCancel.setOnClickListener(v -> dialog.dismiss());
//        EditText etFeedback = dialog.findViewById(R.id.etFeedback);
//
//        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
//        tvConfirm.setOnClickListener(view -> {
//            String Feedback = etFeedback.getText().toString().trim();
//            if (TextUtils.isEmpty(Feedback)) {
////            Toast.makeText(this, "First Name", Toast.LENGTH_SHORT).show();
//                Toast.makeText(getApplicationContext(), "Please enter your feedback", Toast.LENGTH_SHORT).show();
//                return;
//            }
//            Map<String, String> map = new HashMap<>();
//            map.put("feedback", Feedback);
//            submitFeedback(map);
//            dialog.dismiss();
//        });
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//    }


}