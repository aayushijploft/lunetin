package com.social.lunetin.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.social.lunetin.MainActivity;
import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.util;
import com.social.lunetin.network.Apis;
import com.social.lunetin.network.response.ProfileModel;
import com.social.lunetin.network.response.UsernameModel;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateAppActivity extends AppCompatActivity {
    private float x1,x2;
    static final int MIN_DISTANCE = 150;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_app);
        util.setStatusBarGradiant(this);
        findViewById(R.id.ivBack).setOnClickListener(view -> {
            Map<String, String> hashMap1 = new HashMap<>();
            hashMap1.put("rated", "0");
            rateApp(hashMap1,"no");
        });
        findViewById(R.id.btnRate).setOnClickListener(view ->{
            Map<String, String> hashMap1 = new HashMap<>();
            hashMap1.put("rated", "1");
            rateApp(hashMap1,"yes");
        });
    }

    private void launchMarket() {
        final String appPackageName = "com.social.lunetin";
        Uri uri = Uri.parse("market://details?id=" + appPackageName);
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public void rateApp(final Map<String, String> map,String value) {
        final Dialog dialog = new Dialog(RateAppActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<UsernameModel> call = Apis.getAPIService().rateApp("Bearer " + Utility.getToken(),map);
        call.enqueue(new Callback<UsernameModel>() {
            @Override
            public void onResponse(Call<UsernameModel> call, Response<UsernameModel> response) {
                dialog.dismiss();
                UsernameModel user = response.body();
                Log.e("response",user.toString());
                if (user.getStatusCode().equals("200")) {
                    Utility.setRate();
                    if(value.equals("yes")){
                        launchMarket();
                        showDialog();
                    }
                    else if(value.equals("no")){
                        onBackPressed();
                    }

                }
                else if(user.getStatusCode().equals("400")) {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<UsernameModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    onBackPressed();
                    this.overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                }
                else
                {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }
    private void  showDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.got_username);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setOnClickListener(view -> startActivity(new Intent(RateAppActivity.this,GrabUserActivity.class)));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}