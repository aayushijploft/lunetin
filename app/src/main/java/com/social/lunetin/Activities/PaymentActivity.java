package com.social.lunetin.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;


public class PaymentActivity extends AppCompatActivity  {

   String  package_id="",user_id="";
    WebView webView;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        package_id = getIntent().getStringExtra("package_id");
        user_id = Utility.getLoginId();
        webView = findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebViewClient(new WebViewClient());
        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        webView.loadUrl("https://luntin.ga/api/stripeview/"+package_id+"/"+user_id);
        Log.d("aayushiiiiiii", "https://luntin.ga/api/stripeview/"+package_id+"/"+user_id+ "___");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView wView, String url) {
                Log.d("aayushi", url);
                if (url.equals("https://luntin.ga/public/success.php")) //check if that's a url you want to load internally
                {
                    startActivity(new Intent(PaymentActivity.this,ReferActivity.class).putExtra("username","yes"));
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                }
                else if(url.equals("https://luntin.ga/public/fail.php"))
                {
                    Toast.makeText(PaymentActivity.this, "Payment Failed !!!", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }
}