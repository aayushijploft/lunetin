package com.social.lunetin.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.firebase.client.Firebase;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.social.lunetin.MainActivity;
import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.util;
import com.social.lunetin.network.Apis;
import com.social.lunetin.network.response.SignUpModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener , GoogleApiClient.OnConnectionFailedListener{

    EditText et_email,et_password;
    TextView btnLogin;
    RelativeLayout rl_facebook,rl_google;
    LinearLayout layout_createAccount;

//    FACEBOOK
    private CallbackManager callbackManager;
    private LoginManager loginManager;

//    GOOGLE
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 777;
    SignInButton btn_sign_in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        util.setStatusBarGradiant(this);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        btnLogin = findViewById(R.id.btnLogin);
        rl_facebook = findViewById(R.id.rl_facebook);
        rl_google = findViewById(R.id.rl_google);
        layout_createAccount = findViewById(R.id.layout_createAccount);
        layout_createAccount.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        rl_facebook.setOnClickListener(this);
        rl_google.setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        callbackManager = CallbackManager.Factory.create();
        facebookLogin();


    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_createAccount:
                startActivity(new Intent(this,RegisterActivity.class));
                this.overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
                break;
            case R.id.btnLogin:
                submit();
                break;

           case R.id.rl_facebook:
               loginManager.logInWithReadPermissions(this,
                       Arrays.asList("email", "public_profile"));
               break;
        case R.id.rl_google:
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
               break;

        }
    }


    private void facebookLogin() {
        loginManager = LoginManager.getInstance();
        // If you are using in a fragment, call loginButton.setFragment(this);
        // Callback registration
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("LoginActivity", "loginResult = " + loginResult);
                getUserDetails(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e("LoginActivity", "onError = " + exception.getMessage());
            }
        });
    }

    protected void getUserDetails(AccessToken currentAccessToken) {
        String appId = currentAccessToken.getApplicationId();
        GraphRequest data_request = GraphRequest.newMeRequest(currentAccessToken, (json_object, response) -> {
            try {
                String profileUrl = "";
                String email = json_object.get("email").toString();
                String name = json_object.get("name").toString();
                String id = json_object.get("id").toString();

                String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                if (json_object.has("picture")) {
                    profileUrl = json_object.getJSONObject("picture").getJSONObject("data").getString("url");
                }
                Log.e("facebookdata",id+","+profileUrl+","+email);


                Map<String, String> map = new HashMap<>();
                map.put("name", name);
                map.put("email", email);
                map.put("socialtype", "facebook");
                map.put("referCode", "");
                map.put("deviceToken",  Utility.getFirbaseID());
                map.put("socialtoken", id);
                map.put("ip", Utility.getIPaddress());
                SocialSignin(map);
//                Intent intent = new Intent(this, MobileScreen.class);
//                intent.putExtra("email", email);
//                intent.putExtra("id", id);
//                intent.putExtra("profileurl", profileUrl);
//                intent.putExtra("type", "facebook");
//                startActivity(intent);
//                finish();

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "name,email,picture.type(large)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount acct = completedTask.getResult(ApiException.class);
//            Log.d("iiiResult...", "handleSignInResult:" +acct.getEmail());
            String personName = acct.getDisplayName();
            String personPhotoUrl="";
            if(acct.getPhotoUrl()!=null){
                personPhotoUrl = acct.getPhotoUrl().toString();
            }


            String email = acct.getEmail();
            String id = acct.getId();
            String type= "google";
            Log.e("iiiResult...", "Name: " + personName + ", email: " + email + ", Image: " + personPhotoUrl+ "Id: " +id);
                Map<String, String> map = new HashMap<>();
                map.put("name", personName);
                map.put("email", email);
                map.put("socialtype", "google");
                map.put("referCode", "");
                map.put("deviceToken", Utility.getFirbaseID());
                map.put("socialtoken", id);
                map.put("ip", Utility.getIPaddress());
                SocialSignin(map);
//            Intent intent = new Intent(this, MobileScreen.class);
//            intent.putExtra("email", email);
//            intent.putExtra("id", id);
//            intent.putExtra("profileurl", personPhotoUrl);
//            intent.putExtra("type", "google");
//            startActivity(intent);
//            finish();
            // Signed in successfully, show authenticated UI.
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Login", "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    private void submit() {
        // validate
        String email = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Please enter Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!util.isValidEmail(email)) {
            Toast.makeText(getApplicationContext(), "Please enter Valid Email", Toast.LENGTH_SHORT).show();
            return;
        }

        String pass = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(pass)) {
            Toast.makeText(getApplicationContext(), " Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("email", email);
        map.put("password", pass);
        map.put("deviceToken", Utility.getFirbaseID());
        Signin(map);
    }

    public void Signin(final Map<String, String> map) {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<SignUpModel> call = Apis.getAPIService().login(map);
        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dialog.dismiss();
                SignUpModel user = response.body();
                if (user.getStatusCode().equals("400")) {
                   Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (user.getStatusCode().equals("200")) {
                    Utility.setDashBoardData(user.getData());
                    Utility.setlogin();
                    Utility.setloginID(user.getData().getUserId());
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                else {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    public void SocialSignin(final Map<String, String> map) {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<SignUpModel> call = Apis.getAPIService().Sociallogin(map);
        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dialog.dismiss();
                SignUpModel user = response.body();
                if (user.getStatusCode().equals("200")) {
//                    Utility.setDashBoardData(user.getData());
                    Utility.setlogin();
                    Utility.setloginID(user.getData().getUserId());
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                else {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
}