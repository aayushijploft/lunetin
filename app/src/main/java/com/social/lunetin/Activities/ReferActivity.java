package com.social.lunetin.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.util;
import com.social.lunetin.network.Apis;
import com.social.lunetin.network.response.PaymentPageModule;
import com.social.lunetin.network.response.ProfileModel;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferActivity extends AppCompatActivity {

    TextView tvVideoNumber,tvInviteNumber,tvPackageOne,tvPackageTwo;
    private float x1;
    static final int MIN_DISTANCE = 150;
    RelativeLayout rlOne,rlTwo;
    String packageid1="",packageid2="";
    String username="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer);
        util.setStatusBarGradiant(this);
        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        tvVideoNumber = findViewById(R.id.tvVideoNumber);
        tvInviteNumber = findViewById(R.id.tvInviteNumber);
        tvPackageOne = findViewById(R.id.tvPackageOne);
        tvPackageTwo = findViewById(R.id.tvPackageTwo);
        rlOne = findViewById(R.id.rlOne);
        rlTwo = findViewById(R.id.rlTwo);

        if(getIntent().hasExtra("username")){
            username = getIntent().getStringExtra("username");
        }

        findViewById(R.id.layoutInviteFriends).setOnClickListener(view -> {
            startActivity(new Intent(this,InviteFriendsActivity.class));
        });

        findViewById(R.id.rlWatchAds).setOnClickListener(view -> {
            startActivity(new Intent(this,WatchAdsActivity.class));
        });

        rlOne.setOnClickListener(view -> startActivity(new Intent(this,PaymentActivity.class)
                .putExtra("package_id",packageid1)));

        rlTwo.setOnClickListener(view -> startActivity(new Intent(this,PaymentActivity.class)
                .putExtra("package_id",packageid2)));

        getPaymentPage();

        if(username.equals("yes")){
            showDialog();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                float x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    // Toast.makeText(this, "left2right swipe", Toast.LENGTH_SHORT).show ();
                    onBackPressed();
//                    startActivity(new Intent(GrabUserActivity.this, MainActivity.class)
//                            .putExtra("name",tvUserName.getText().toString())
//                            .putExtra("id",recuid));
//                    this.overridePendingTransition(R.anim.anim_slide_in_right,
//                            R.anim.anim_slide_out_right);
                    this.overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                }
                else
                {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public void getPaymentPage() {
        Call<PaymentPageModule> call = Apis.getAPIService().getPaymentPage("Bearer " + Utility.getToken());
        call.enqueue(new Callback<PaymentPageModule>() {
            @Override
            public void onResponse(Call<PaymentPageModule> call, Response<PaymentPageModule> response) {
                PaymentPageModule user = response.body();

                Log.e("response",user.toString());

                if (user.getStatusCode().equals("200")) {
                    tvVideoNumber.setText(user.getData().getWatchvideo()+"/"+user.getData().getTotalvideo());
                    tvInviteNumber.setText(user.getData().getInvited()+"/"+user.getData().getTotalinvite());
                    tvPackageOne.setText("$"+user.getData().getPackage1().getAmount());
                    tvPackageTwo.setText("$"+user.getData().getPackage2().getAmount());
                    packageid1=user.getData().getPackage1().getPackageidandroid();
                    packageid2=user.getData().getPackage2().getPackageidandroid();
                }
                else if(user.getStatusCode().equals("400")) {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PaymentPageModule> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }

        });
    }

    private void showDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.got_username);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setOnClickListener(view -> startActivity(new Intent(ReferActivity.this,GrabUserActivity.class)));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


}