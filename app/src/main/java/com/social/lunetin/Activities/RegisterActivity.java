package com.social.lunetin.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;
import com.google.android.material.snackbar.Snackbar;
import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.util;
import com.social.lunetin.network.Apis;
import com.social.lunetin.network.response.SignUpModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_name, et_email, et_password;
    TextView btn_CreateAccount;
    LinearLayout layout_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        util.setStatusBarGradiant(this);
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        btn_CreateAccount = findViewById(R.id.btn_CreateAccount);
        layout_login = findViewById(R.id.layout_login);
        layout_login.setOnClickListener(this);
        btn_CreateAccount.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_login:
                startActivity(new Intent(this, LoginActivity.class));
                this.overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
                break;
            case R.id.btn_CreateAccount:
                submit();
                break;

        }
    }


    private void submit() {
        // validate
        String Name = et_name.getText().toString().trim();
        if (TextUtils.isEmpty(Name)) {
//            Toast.makeText(this, "First Name", Toast.LENGTH_SHORT).show();
            Toast.makeText(getApplicationContext(), "Please enter your first name", Toast.LENGTH_SHORT).show();
            return;
        }

        String email = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Please enter Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!util.isValidEmail(email)) {
            Toast.makeText(getApplicationContext(), "Please enter Valid Email", Toast.LENGTH_SHORT).show();
            return;
        }

        String pass = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(pass)) {
            Toast.makeText(getApplicationContext(), " Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("name", Name);
        map.put("email", email);
        map.put("password", pass);
        map.put("referCode", "");
        map.put("deviceToken", Utility.getFirbaseID());
        map.put("ip", Utility.getIPaddress());
        Signup(map);
    }

    public void Signup(final Map<String, String> map) {
        final Dialog dialog = new Dialog(RegisterActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<SignUpModel> call = Apis.getAPIService().register(map);
        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dialog.dismiss();
                SignUpModel user = response.body();
                if (user.getStatusCode().equals("200")) {
                    Utility.setDashBoardData(user.getData());
                    Utility.setlogin();
                    Utility.setloginID(user.getData().getUserId());
                    startActivity(new Intent(RegisterActivity.this, SelectUsernameActivity.class));
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                else if (user.getStatusCode().equals("400")) {
                    Toast.makeText(RegisterActivity.this,  user.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

//    public void showmessage(String message) {
//        View parentLayout = findViewById(android.R.id.content);
//        Snackbar snack = Snackbar.make(snackbar, message, Snackbar.LENGTH_LONG);
//        View snackBarView = snack.getView();
//        // snackBarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
//        snackBarView.setBackgroundResource(R.drawable.edtbackground);
//
//        snack.setAction("CLOSE", new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
//        snack.setActionTextColor(getResources().getColor(android.R.color.black));
//        snack.show();
//
//
//    }

}