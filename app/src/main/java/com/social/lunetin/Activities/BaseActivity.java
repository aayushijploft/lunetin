package com.social.lunetin.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.social.lunetin.R;
import com.social.lunetin.Utils.util;

import okhttp3.internal.Util;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        util.setOnlineStatus(BaseActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        util.setOfflineStatus(BaseActivity.this);
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        util.setOfflineStatus(BaseActivity.this);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        util.setOnlineStatus(BaseActivity.this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        util.setOnlineStatus(BaseActivity.this);
    }
}