package com.social.lunetin.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.util;
import com.social.lunetin.network.Apis;
import com.social.lunetin.network.response.ProfileModel;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InviteFriendsActivity extends AppCompatActivity {
    TextView tvReferLink,tvReportaproblem,tvUserName;
    ImageView ivWhatsapp,ivFacebook,ivTwitter,ivGmail;
    ProfileModel user;
    private float x1;
    static final int MIN_DISTANCE = 150;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);
        util.setStatusBarGradiant(this);
        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        tvReferLink=findViewById(R.id.tvReferLink);
        ivFacebook=findViewById(R.id.ivFacebook);
        tvReportaproblem=findViewById(R.id.tvReportaproblem);
        tvUserName=findViewById(R.id.tvUserName);
        getProfile();
        tvReferLink.setOnClickListener(view ->{
            setClipboard(this,tvReferLink.getText().toString());
            Log.e("copied",tvReferLink.getText().toString());
        });

        ivWhatsapp = findViewById(R.id.ivWhatsapp);
        ivTwitter = findViewById(R.id.ivTwitter);
        ivGmail = findViewById(R.id.ivGmail);

        ivWhatsapp.setOnClickListener(view -> {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Join me on LuneTin, " +
                    "an application that allows you to chat with similar talents people.\n\n"+
                    user.getData().getUserdetail().getReferUrl());
            try {
                startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();
            }
        });

        ivFacebook.setOnClickListener(view -> {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            whatsappIntent.setPackage("com.facebook.katana");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Join me on LuneTin, " +
                    "an application that allows you to chat with similar talents people.\n\n"+
                    user.getData().getUserdetail().getReferUrl());
            try {
                startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "Facebook have not been installed.", Toast.LENGTH_SHORT).show();
            }
        });

        ivTwitter.setOnClickListener(view -> {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            whatsappIntent.setPackage("com.twitter.android");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Join me on LuneTin, " +
                    "an application that allows you to chat with similar talents people.\n\n"+
                    user.getData().getUserdetail().getReferUrl());
            try {
                startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "Twitter have not been installed.", Toast.LENGTH_SHORT).show();
            }
        });

        ivGmail.setOnClickListener(view -> {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto","", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Invitation for LuneTin");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Join me on LuneTin, " +
                    "an application that allows you to chat with similar talents people.\n\n"+
                    user.getData().getUserdetail().getReferUrl());
            startActivity(Intent.createChooser(emailIntent, "Send Email..."));

        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                float x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    onBackPressed();
                    this.overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                }
                // consider as something else - a screen tap for example

                break;
        }
        return super.onTouchEvent(event);
    }

    public void getProfile() {

        Call<ProfileModel> call = Apis.getAPIService().getProfile("Bearer " + Utility.getToken());
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(@NotNull Call<ProfileModel> call, @NotNull Response<ProfileModel> response) {

                 user = response.body();
                assert user != null;
                Log.e("response",user.toString());
                if (user.getStatusCode().equals("200")) {
                    tvReferLink.setText(user.getData().getUserdetail().getReferUrl());
                    tvReferLink.setText(user.getData().getUserdetail().getReferUrl());
                    tvReportaproblem.setText(user.getData().getInvited());
                    tvUserName.setText(user.getData().getInvited()); }
                else if(user.getStatusCode().equals("400")) {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProfileModel> call, @NotNull Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @SuppressLint("ObsoleteSdkInt")
    private void setClipboard(Context context, String text) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            Toast.makeText(context, "Copied Text", Toast.LENGTH_SHORT).show();
            clipboard.setPrimaryClip(clip);
        }
    }

}