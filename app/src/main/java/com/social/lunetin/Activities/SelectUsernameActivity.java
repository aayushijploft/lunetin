package com.social.lunetin.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;
import com.social.lunetin.Adapters.UsernameAdapter;
import com.social.lunetin.MainActivity;
import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.util;
import com.social.lunetin.network.Apis;
import com.social.lunetin.network.response.AddUserModel;
import com.social.lunetin.network.response.SignUpModel;
import com.social.lunetin.network.response.UserSuggestionModel;
import com.social.lunetin.network.response.UsernameModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectUsernameActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rvUserName;
    TextView btn_Selectusername,tvTitle,tvmessage;
    EditText et_name;
    UsernameAdapter usernameAdapter;
    UserSuggestionModel user;
    ImageView ivRefresh;
    ArrayList<String> list = new ArrayList<>();
    private float x1;
    static final int MIN_DISTANCE = 150;
    TextView btnuserlist,tvSuggestionText,tvSuggestionmsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_username);
        util.setStatusBarGradiant(this);
        rvUserName = findViewById(R.id.rvUserName);
        btn_Selectusername = findViewById(R.id.btn_Selectusername);
        et_name = findViewById(R.id.et_name);
        tvTitle = findViewById(R.id.tvTitle);
        tvmessage = findViewById(R.id.tvmessage);
        ivRefresh = findViewById(R.id.ivRefresh);
        btnuserlist = findViewById(R.id.btnuserlist);
        tvSuggestionText = findViewById(R.id.tvSuggestionText);
        tvSuggestionmsg = findViewById(R.id.tvSuggestionmsg);

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    Map<String, String> map = new HashMap<>();
                    map.put("text", charSequence.toString());
                    Log.e("map",map.toString());
                    getUsernameSuggestions1(map);
//                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        rvUserName.setHasFixedSize(true);
        rvUserName.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        btnuserlist.setOnClickListener(view -> showUserListDialog());
        ivRefresh.setOnClickListener(view -> {
            finish();
            startActivity(getIntent());
        });

        btn_Selectusername.setOnClickListener(this);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                float x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    onBackPressed();
                    this.overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                }
                break;
        }
        return super.onTouchEvent(event);
    }


    private void showNoteDialog(String name) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.note_dialog_layout);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());

        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setOnClickListener(view -> {
            dialog.dismiss();
//            if(list.contains(et_name.getText().toString().trim())){

//            }

//            Toast.makeText(this, user.getShowpopup(), Toast.LENGTH_SHORT).show();
            if(user.getShowpopup().equals("1")){
                showUsernameAlertDialog();
            }
            else {
                Map<String, String> map = new HashMap<>();
                map.put("name", name);
//                map.put("firebase_user_id",Utility.getLoginId());
                map.put("firebase_user_id", Utility.getLoginId()+"_"+name);
                Log.e("map",map.toString());
                addUsername(map,name);

            }

        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showUsernameAlertDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.username_alert_dailog);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());

        TextView tvOk = dialog.findViewById(R.id.tvOk);
        TextView tvEdit = dialog.findViewById(R.id.tvEdit);
        tvOk.setOnClickListener(view -> {
            dialog.dismiss();
            Map<String, String> map = new HashMap<>();
            map.put("name", et_name.getText().toString().trim());
            map.put("firebase_user_id", Utility.getLoginId()+"_"+et_name.getText().toString().trim());
            Log.e("map",map.toString());
            addUsername(map,et_name.getText().toString().trim());
        });

        tvEdit.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showUserListDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.userlist_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        ImageView ivRefresh =dialog.findViewById(R.id.ivRefresh);
        EditText et_name =dialog.findViewById(R.id.et_name);
        TextView btn_Selectusername = dialog.findViewById(R.id.btn_Selectusername);
        imgCancel.setOnClickListener(v -> dialog.dismiss());
        RecyclerView rvUserName = dialog.findViewById(R.id.rvUserName);
        rvUserName.setHasFixedSize(true);
        rvUserName.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        ivRefresh.setOnClickListener(view -> {
            et_name.setText("");
            Map<String, String> map = new HashMap<>();
            map.put("text", "");
            Log.e("map",map.toString());
            getUsernameSuggestions(map,rvUserName,et_name);
        });
        btn_Selectusername.setOnClickListener(view -> showNoteDialog(et_name.getText().toString()));
        Map<String, String> map = new HashMap<>();
        map.put("text", "");
        Log.e("map",map.toString());
        getUsernameSuggestions(map,rvUserName,et_name);
        et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Map<String, String> map = new HashMap<>();
                map.put("text", charSequence.toString());
                Log.e("map",map.toString());
                getUsernameSuggestions(map,rvUserName,et_name);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_Selectusername) {
           showNoteDialog(et_name.getText().toString());
        }
    }

    public void getUsernameSuggestions(final Map<String, String> map,RecyclerView rvUserName,EditText et_name) {
        Call<UserSuggestionModel> call = Apis.getAPIService().getUserSuggestions("Bearer " + Utility.getToken(),map);
        call.enqueue(new Callback<UserSuggestionModel>() {
            @Override
            public void onResponse(Call<UserSuggestionModel> call, Response<UserSuggestionModel> response) {
                 user = response.body();
                Log.e("response",user.toString());
                if (user.getStatusCode().equals("200")) {
                        if(user.getData() != null){
                            for (int i=0;i<user.getData().size();i++){
                                list.add(user.getData().get(i).getUsername());
                            }

                            if(user.getData().size()>0){
                                rvUserName.setVisibility(View.VISIBLE);
                                usernameAdapter = new UsernameAdapter(SelectUsernameActivity.this,user.getData(),pos -> {
                                    et_name.setText(user.getData().get(pos).getUsername());
                                    showNoteDialog(et_name.getText().toString());
                                });
                                rvUserName.setAdapter(usernameAdapter);
                            }
                            else rvUserName.setVisibility(View.GONE);
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                    }
            }

            @Override
            public void onFailure(Call<UserSuggestionModel> call, Throwable t) {
//                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
    public void getUsernameSuggestions1(final Map<String, String> map) {
        Call<UserSuggestionModel> call = Apis.getAPIService().getUserSuggestions("Bearer " + Utility.getToken(),map);
        call.enqueue(new Callback<UserSuggestionModel>() {
            @Override
            public void onResponse(Call<UserSuggestionModel> call, Response<UserSuggestionModel> response) {
//                dialog.dismiss();
                 user = response.body();
                Log.e("response",user.toString());
                if (user.getStatusCode().equals("200")) {
                        if(user.getData() != null){
                            for (int i=0;i<user.getData().size();i++){
                                list.add(user.getData().get(i).getUsername());
                            }
                            if(user.getData().size()>0){
                                rvUserName.setVisibility(View.VISIBLE);
                                tvSuggestionText.setVisibility(View.VISIBLE);
                                tvSuggestionmsg.setVisibility(View.VISIBLE);
                                usernameAdapter = new UsernameAdapter(SelectUsernameActivity.this,user.getData(),pos -> {
                                    et_name.setText(user.getData().get(pos).getUsername());
                                    showNoteDialog(et_name.getText().toString());
                                });
                                rvUserName.setAdapter(usernameAdapter);
                            }
                            else rvUserName.setVisibility(View.GONE);
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                    }
            }

            @Override
            public void onFailure(Call<UserSuggestionModel> call, Throwable t) {
//                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void addUsername(final Map<String, String> map,String name) {
        final Dialog dialog = new Dialog(SelectUsernameActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Call<AddUserModel> call = Apis.getAPIService().addUsername("Bearer " + Utility.getToken(),map);
        call.enqueue(new Callback<AddUserModel>() {
            @Override
            public void onResponse(Call<AddUserModel> call, Response<AddUserModel> response) {
                dialog.dismiss();
                AddUserModel user = response.body();
                Log.e("response",user.toString());
                switch (user.getStatusCode()) {
                    case "200":
                        setUserForChat(Utility.getLoginId() + "_" + name, name);
                        Utility.setUserName(user.getData().getUsername());
                        Utility.setUserNameId(user.getData().getId());
                        Log.e("qwert", Utility.getUserNameId() + "_" + et_name.getText().toString());

                        startActivity(new Intent(SelectUsernameActivity.this, MainActivity.class)
                                .putExtra("name", user.getData().getUsername())
                                .putExtra("id", user.getData().getId()));

                        break;
                    case "400":
                        Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                        break;
                    case "600":
                        showUserDialog(user.getMessage());
                        break;
                }
            }

            @Override
            public void onFailure(Call<AddUserModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private void setUserForChat(String userId,String name) {
        Firebase.setAndroidContext(this);
        String url = "https://lunetin-59aa3-default-rtdb.firebaseio.com/users.json";
        StringRequest request = new StringRequest(Request.Method.GET, url, s -> {
            Firebase reference = new Firebase("https://lunetin-59aa3-default-rtdb.firebaseio.com/users");

            if (s.equals("null")) {
                reference.child(userId).child("userID").setValue(Utility.getLoginId());
                reference.child(userId).child("userName").setValue(name);
            } else {
                try {
                    JSONObject obj = new JSONObject(s);
                    if (!obj.has(userId)) {
                        reference.child(userId).child("userID").setValue(Utility.getLoginId());
                        reference.child(userId).child("userName").setValue(name);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, volleyError -> {
            System.out.println("" + volleyError);
        });

        RequestQueue rQueue = Volley.newRequestQueue(this);
        rQueue.add(request);
    }

    private void showUserDialog(String message) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.username_alert_dailog);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());

        TextView tvOk = dialog.findViewById(R.id.tvOk);
        TextView tvmessage = dialog.findViewById(R.id.tvmessage);
        TextView tvline = dialog.findViewById(R.id.tvline);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvTitle.setText("Username Full");
        tvmessage.setText(message);
        TextView tvEdit = dialog.findViewById(R.id.tvEdit);
        tvEdit.setVisibility(View.GONE);
        tvline.setVisibility(View.GONE);
        imgCancel.setVisibility(View.GONE);
        tvOk.setOnClickListener(view -> {
            dialog.dismiss();
        });
        tvEdit.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}