package com.social.lunetin.Activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.social.lunetin.MainActivity;
import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.WebAppInterface;
import com.social.lunetin.Utils.util;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {

    String Device_Key = "";
    WebView webview;

    @SuppressLint({"JavascriptInterface", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);


        util.setStatusBarGradiant(this);
        printHashKey(this);
        getKey();
        new Handler().postDelayed(() -> {
            try {
                getJSONObjectFromURL("https://luntin.ga/share/1?type=hidden");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, 1000);
    }

    public void getJSONObjectFromURL(String urlString) throws IOException {

        HttpURLConnection urlConnection = null;

        URL url = new URL(urlString);

        urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(1000 /* milliseconds */);
        urlConnection.setConnectTimeout(1000 /* milliseconds */);

        urlConnection.setDoOutput(true);

        urlConnection.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        char[] buffer = new char[1024];

        String jsonString = new String();

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        jsonString = sb.toString();

        urlConnection.disconnect();
        Log.d("asdfasdfasdf", jsonString + "____");
        Utility.setIPAddresss(jsonString + "");
        if (Utility.isLogin()) {
            startActivity(new Intent(this, MainActivity.class));
        } else startActivity(new Intent(this, LoginActivity.class));

    }

    private void getKey() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener
                (SplashActivity.this, instanceIdResult -> {
                    Device_Key = instanceIdResult.getToken();
                    Utility.setFirbaseID(Device_Key);
                    Log.e("key", Device_Key);
                });
    }

    public static void printHashKey(Context pContext) {
        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.d("Hash Key: ", hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.d("printHashKey()", e.toString());
        } catch (Exception e) {
            Log.e("printHashKey()", e.toString());
        }
    }

    static class MyJavaScriptInterface {

        private Context ctx;

        MyJavaScriptInterface(Context ctx) {
            this.ctx = ctx;
        }

        public void showHTML(String html) {
            new AlertDialog.Builder(ctx).setTitle("HTML").setMessage(html)
                    .setPositiveButton(android.R.string.ok, null).setCancelable(false).create().show();
        }

    }

}