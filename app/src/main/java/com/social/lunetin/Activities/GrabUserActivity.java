package com.social.lunetin.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.social.lunetin.Adapters.ChatListAdapter;
import com.social.lunetin.Adapters.GrabUsernameAdapter;
import com.social.lunetin.MainActivity;
import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.util;
import com.social.lunetin.network.Apis;
import com.social.lunetin.network.response.ProfileModel;
import com.social.lunetin.network.response.UsernameModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GrabUserActivity extends AppCompatActivity {

    RecyclerView rvGrabUNList;
    TextView tvUserName;
    private float x1,x2;
    static final int MIN_DISTANCE = 150;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grab_user);
        util.setStatusBarGradiant(this);
        findViewById(R.id.ivBack).setOnClickListener(view -> {
            onBackPressed();
            this.overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        });
        rvGrabUNList = findViewById(R.id.rvGrabUNList);
        tvUserName = findViewById(R.id.tvUserName);
        rvGrabUNList.setHasFixedSize(true);
        rvGrabUNList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        getProfile();

        findViewById(R.id.btnAddUsername).setOnClickListener(view -> startActivity(new Intent(this, ReferActivity.class)));

    }
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                   // Toast.makeText(this, "left2right swipe", Toast.LENGTH_SHORT).show ();
                        onBackPressed();
//                    startActivity(new Intent(GrabUserActivity.this, MainActivity.class)
//                            .putExtra("name",tvUserName.getText().toString())
//                            .putExtra("id",recuid));
//                    this.overridePendingTransition(R.anim.anim_slide_in_right,
//                            R.anim.anim_slide_out_right);
                    this.overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                }
                else
                {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }
    public void getProfile() {
        final Dialog dialog = new Dialog(GrabUserActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<ProfileModel> call = Apis.getAPIService().getProfile("Bearer " + Utility.getToken());
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dialog.dismiss();
                ProfileModel user = response.body();
                Log.e("response", user.toString());
                if (user.getStatusCode().equals("200")) {
                    for (int i = 0; i < Integer.parseInt(user.getData().getUserdetail().getRemail()); i++) {
                    if(Integer.parseInt(user.getData().getUserdetail().getRemail()) > 0){
                        ProfileModel.Usernamelist usernamelist = new ProfileModel.Usernamelist();
                        usernamelist.setId("test");
                        usernamelist.setChatcount("test");
                        usernamelist.setChatcount("test");
                        usernamelist.setFirebase_user_id("test");
                        usernamelist.setSelected("test");
                        usernamelist.setUser_id("test");
                        usernamelist.setUsername("Choose a username");
//                        (remaining "+user.getData().getUserdetail().getRemail()+")
                        user.getData().getUserdetail().getUsernamelist().add(usernamelist);
                    }

                    }
                    String upperString = user.getData().getUserdetail().getUsernamelist().get(0).getUsername().substring(0, 1).toUpperCase()
                            + user.getData().getUserdetail().getUsernamelist().get(0).getUsername().substring(1).toLowerCase();
                    tvUserName.setText(upperString);
                    GrabUsernameAdapter grabUsernameAdapter = new GrabUsernameAdapter(GrabUserActivity.this, user.getData().getUserdetail().getUsernamelist(),pos -> {
//                        Toast.makeText(GrabUserActivity.this, user.getData().getUserdetail().getUsernamelist().get(pos).getUsername(), Toast.LENGTH_SHORT).show();
                        if(user.getData().getUserdetail().getUsernamelist().get(pos).getUsername().equals("Choose a username")){
                            startActivity(new Intent(GrabUserActivity.this,SelectUsernameActivity.class));
                        }
                        else {
//                            util.setOfflineStatus(GrabUserActivity.this);
//                            Utility.setUserNameId(user.getData().getUserdetail().getUsernamelist().get(pos).getId());
                            startActivity(new Intent(GrabUserActivity.this,MainActivity.class)
                                    .putExtra("name",user.getData().getUserdetail().getUsernamelist().get(pos).getUsername())
                                    .putExtra("id",user.getData().getUserdetail().getUsernamelist().get(pos).getId())
                            );
                            Utility.setUserName(user.getData().getUserdetail().getUsernamelist().get(pos).getUsername());
                            Utility.setUserNameId(user.getData().getUserdetail().getUsernamelist().get(pos).getId());



                        }
                    });
                    rvGrabUNList.setAdapter(grabUsernameAdapter);
                } else if (user.getStatusCode().equals("400")) {
                    Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

}