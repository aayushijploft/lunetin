package com.social.lunetin.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.social.lunetin.MainActivity;
import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;
import com.social.lunetin.Utils.util;
import com.social.lunetin.network.Apis;
import com.social.lunetin.network.response.UsernameModel;
import com.social.lunetin.network.response.WatchadModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WatchAdsActivity extends AppCompatActivity {

    private float x1,x2;
    static final int MIN_DISTANCE = 150;
    private InterstitialAd interstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_ads);
        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());
        //initializing the Google Admob SDK
        MobileAds.initialize (this, initializationStatus -> {

            //Showing a simple Toast Message to the user when The Google AdMob Sdk Initialization is Completed

     //       Toast.makeText (WatchAdsActivity.this, "AdMob Sdk Initialize "+ initializationStatus.toString(), Toast.LENGTH_LONG).show();

        });


        //Initializing the InterstitialAd  objects
        interstitialAd = new InterstitialAd (WatchAdsActivity.this) ;

        interstitialAd.setAdUnitId ( "ca-app-pub-3153556820784156/4324798755" ) ;

        interstitialAd.setAdListener( new AdListener() {
            @Override
            public void onAdLoaded() {

                // Showing a simple Toast message to user when an ad is loaded
                showInterstitialAd();
//                Toast.makeText ( WatchAdsActivity.this, "Interstitial Ad is Loaded", Toast.LENGTH_LONG).show() ;

            }

            @Override
            public void onAdFailedToLoad( LoadAdError adError) {

                // Showing a simple Toast message to user when and ad is failed to load
//                Toast.makeText ( WatchAdsActivity.this, "Interstitial Ad Failed to Load ", Toast.LENGTH_LONG).show() ;

            }

            @Override
            public void onAdOpened() {

                // Showing a simple Toast message to user when an ad opens and overlay and covers the device screen
//                Toast.makeText ( WatchAdsActivity.this, "Interstitial Ad Opened", Toast.LENGTH_LONG).show() ;

            }

            @Override
            public void onAdClicked() {

                // Showing a simple Toast message to user when a user clicked the ad
//                Toast.makeText ( WatchAdsActivity.this, "Interstitial Ad Clicked", Toast.LENGTH_LONG).show() ;

            }

            @Override
            public void onAdLeftApplication() {

                // Showing a simple Toast message to user when the user left the application
//                Toast.makeText ( WatchAdsActivity.this, "Interstitial Ad Left the Application", Toast.LENGTH_LONG).show() ;

            }

            @Override
            public void onAdClosed() {

                //loading new interstitialAd when the ad is closed
//                loadInterstitialAd() ;
                watchAd();

                // Showing a simple Toast message to user when the user interacted with ad and got the other app and then return to the app again
//                Toast.makeText ( WatchAdsActivity.this, "Interstitial Ad is Closed", Toast.LENGTH_LONG).show() ;

            }
        });

        loadInterstitialAd();

    }

    private void loadInterstitialAd()
    {
        // Creating  a Ad Request
        AdRequest adRequest = new AdRequest.Builder().build() ;

        // load Ad with the Request
        interstitialAd.loadAd(adRequest) ;
//        showInterstitialAd();
        // Showing a simple Toast message to user when an ad is Loading
//        Toast.makeText ( WatchAdsActivity.this, "Interstitial Ad is loading ", Toast.LENGTH_LONG).show() ;

    }

    private void showInterstitialAd()
    {
        if ( interstitialAd.isLoaded() )
        {
            //showing the ad Interstitial Ad if it is loaded
            interstitialAd.show() ;

            // Showing a simple Toast message to user when an Interstitial ad is shown to the user
//            Toast.makeText ( WatchAdsActivity.this, "Interstitial is loaded and showing ad  ", Toast.LENGTH_LONG).show() ;

        }
        else
        {
            //Load the Interstitial ad if it is not loaded
            loadInterstitialAd() ;

            // Showing a simple Toast message to user when an ad is not loaded
//            Toast.makeText ( WatchAdsActivity.this, "Interstitial Ad is not Loaded ", Toast.LENGTH_LONG).show() ;

        }

    }
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE)
                {
                    // Toast.makeText(this, "left2right swipe", Toast.LENGTH_SHORT).show ();
                    onBackPressed();
//                    startActivity(new Intent(GrabUserActivity.this, MainActivity.class)
//                            .putExtra("name",tvUserName.getText().toString())
//                            .putExtra("id",recuid));
//                    this.overridePendingTransition(R.anim.anim_slide_in_right,
//                            R.anim.anim_slide_out_right);
                    this.overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                }
                else
                {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public void watchAd() {
        Call<WatchadModel> call = Apis.getAPIService().watchAd("Bearer " + Utility.getToken());
        call.enqueue(new Callback<WatchadModel>() {
            @Override
            public void onResponse(Call<WatchadModel> call, Response<WatchadModel> response) {
                WatchadModel user = response.body();
                if (user.getStatusCode().equals("200")) {

                    if(user.getAdcount().equals(user.getRequiread())){
                        startActivity(new Intent(WatchAdsActivity.this,ReferActivity.class).putExtra("username","yes"));
                        overridePendingTransition(R.anim.anim_slide_in_right,
                                R.anim.anim_slide_out_right);
                    }
                    else {
                        startActivity(new Intent(WatchAdsActivity.this,ReferActivity.class).putExtra("username","no"));
                        overridePendingTransition(R.anim.anim_slide_in_right,
                                R.anim.anim_slide_out_right);
                    }

                }
                else if(user.getStatusCode().equals("400")) {
                    // Toast.makeText(getApplicationContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<WatchadModel> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

}