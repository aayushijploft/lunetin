package com.social.lunetin.Notification_new;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.social.lunetin.R;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class FirebaseMessageReceive extends FirebaseMessagingService {
    private static final String TAG ="vishal_data";
    public static int NOTIFICATION_ID;
    NotificationCompat.Builder builder;
    Intent intent;
    String body = "",title ="",userID ="",userType="", TicketId ="",project_id="";
    private NotificationManager mNotificationManager;
    String user_id = "";
    String name = "";
    String click_action = "";
    private NotificationUtils notificationUtils;
    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.d(TAG, "NEW_TOKEN: " + token);

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Log.d(TAG, "Message data payload123242: " + remoteMessage.getNotification());
            Map<String, String> data = remoteMessage.getData();
            if (remoteMessage.getData() != null || !remoteMessage.getData().isEmpty()) {
                Log.d(TAG, "Message data payload123242: " + remoteMessage.getData());

                if (remoteMessage.getData() != null || !remoteMessage.getData().isEmpty()) {
                    body = data.get("body");
                    title =  remoteMessage.getData().get("title");
                    user_id = remoteMessage.getData().get("UserId");
                    if(remoteMessage.getData().containsKey("project_id")){
                        project_id = remoteMessage.getData().get("project_id");
                    }

                    name = remoteMessage.getData().get("name");
                    click_action = remoteMessage.getData().get("click_action");
                    Log.d(TAG, "From: " + user_id + "_" + name);
                }
                handleNotification(body, title, user_id,name,click_action);
            } else {
                if (remoteMessage.getData() == null || remoteMessage.getData().isEmpty()) {
                    String messageNotification = remoteMessage.getNotification().getBody();
                    if (messageNotification != null) {
                        handleNotification(messageNotification, remoteMessage.getNotification().getTitle(),"","",click_action);
                    }
                }
                else
                {
                    if (remoteMessage.getData() != null || !remoteMessage.getData().isEmpty()) {
                            body = data.get("body");
                            title =  remoteMessage.getData().get("title");
                            user_id = remoteMessage.getData().get("UserId");
                            name = remoteMessage.getData().get("name");
                        click_action = remoteMessage.getData().get("click_action");
                            Log.d(TAG, "From: " + user_id + "_" + name);
                        }
                        handleNotification(body, title,  user_id,name,click_action);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private void handleNotification(String message, String title, String User_Id, String Name, String click_action) {

        Random rand = new Random();
        int n = rand.nextInt(5000);

        if (!isAppIsInBackground(this)) {
            try {

                PendingIntent contentIntent = null;
                Intent intent;


                AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                manager.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0);
                Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.notification_sound);
               /* MediaPlayer player = MediaPlayer.create(getApplicationContext(), uri);
                player.start();*/

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        if (Build.VERSION.SDK_INT < 26) {
                            return;
                        }
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        NotificationChannel channel = new NotificationChannel("default", "Channel name", NotificationManager.IMPORTANCE_DEFAULT);
                        channel.setDescription("Channel description");
                        notificationManager.createNotificationChannel(channel);
                    }
                    builder = new NotificationCompat.Builder(this, "default")
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.app_logo))
                            .setSmallIcon(R.drawable.app_logo)
                            .setColor(getResources().getColor(R.color.black))
                            .setColor(Color.TRANSPARENT)
                            .setContentText(message)
                            .setSound(uri)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_MAX)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message));

                /*NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();*/
                } else {
                    builder = new NotificationCompat.Builder(this)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.app_logo))
                            .setSmallIcon(R.drawable.app_logo)
                            .setContentText(message)
                            .setSound(uri)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_MAX)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message));

                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.playNotificationSound();
                }

                builder.setContentIntent(contentIntent);
                mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

//                builder.setDefaults(NotificationCompat.DEFAULT_ALL);
                mNotificationManager.notify(n, builder.build());

            } catch (Exception e) {
                e.printStackTrace();
            }

            Intent pushNotification = new Intent("pushNotification");
            pushNotification.putExtra("isNotification", true);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

        /*NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();*/
        } else {
//            try {
//                PendingIntent contentIntent;
//                Intent intent;
//                switch (click_action) {
//                    case "notificationLike":
//                        intent = new Intent(this, OtherUserProfileActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        intent.putExtra("UserId", User_Id);
//
//                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//                        break;
//                    case "notificationTalant":
//                        intent = new Intent(this, OtherUserProfileActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        intent.putExtra("UserId", User_Id);
//
//                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//                        break;
//                    case "notificationProject":
//                        intent = new Intent(this, ViewAssignment.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        intent.putExtra("project_id", project_id);
//                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//                        break;
//                    default:
//                        intent = new Intent(this, ChatActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        intent.putExtra("UserId", User_Id);
//                        intent.putExtra("name", Name);
//
//                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//                        break;
//                }
//
//
//
//                AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//                manager.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0);
//                Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.notification_sound);
//
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        if (Build.VERSION.SDK_INT < 26) {
//                            return;
//                        }
//                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                        NotificationChannel channel = new NotificationChannel("default", "Channel name", NotificationManager.IMPORTANCE_DEFAULT);
//                        channel.setDescription("Channel description");
//                        notificationManager.createNotificationChannel(channel);
//                    }
//                    builder = new NotificationCompat.Builder(this, "default")
//                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.tiecoon_logo))
//                            .setSmallIcon(R.drawable.tiecoon_logo)
//                            .setColor(getResources().getColor(R.color.colorPrimary))
//                            .setColor(Color.TRANSPARENT)
//                            .setContentText(message)
//                            .setSound(uri)
//                            .setContentTitle(title)
//                            .setAutoCancel(true)
//                            .setPriority(Notification.PRIORITY_MAX)
//                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message));
//
//
//                } else {
//                    builder = new NotificationCompat.Builder(this)
//                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.tiecoon_logo))
//                            .setSmallIcon(R.drawable.tiecoon_logo)
//                            .setContentText(message)
//                            .setSound(uri)
//                            .setContentTitle(title)
//                            .setAutoCancel(true)
//                            .setPriority(Notification.PRIORITY_MAX)
//                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message));
//
//                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//                    notificationUtils.playNotificationSound();
//                }
//
//                builder.setContentIntent(contentIntent);
//
//                mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//
//                mNotificationManager.notify(n, builder.build());
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            Intent pushNotification = new Intent("pushNotification");
            pushNotification.putExtra("isNotification", true);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

}
