package com.social.lunetin.Notification_new;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.social.lunetin.Activities.ChatActivity;
import com.social.lunetin.Activities.GrabUserActivity;
import com.social.lunetin.MainActivity;
import com.social.lunetin.R;
import com.social.lunetin.Utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    String channelId;
    public static final String SHARED_PREF = "Firbase_Token";
    String user_id = "";
    String name = "";
    String firebaseUserId = "";
    String hexCodeTop = "#e094bb";
    String id = "";
    String senderId = "";
    String username_user_id = "";
    String blockstatus = "";
    String blockby = "";
    String username = "";
    String image1;
    Bitmap image;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
        try {
            RemoteMessage.Notification params = remoteMessage.getNotification();
            JSONObject object = new JSONObject(String.valueOf(params));
            Log.e("JSON_OBJECT", object.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                // scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }
        Log.i(TAG, "onMessageReceived: title : " + remoteMessage.getNotification());

        // Check if message contains a notification payload.
        if (remoteMessage.getData().size() > 0) {
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("usernamedetail"));
                username_user_id = jsonObject.getString("username_user_id");
                blockstatus = jsonObject.getString("blockstatus");
                firebaseUserId = jsonObject.getString("firebaseUserId");
                if (hexCodeTop != null) {
                    hexCodeTop = jsonObject.getString("hexCodeTop");
                }

                id = jsonObject.getString("id");
                senderId = jsonObject.getString("senderId");
                user_id = jsonObject.getString("user_id");
                blockby = jsonObject.getString("blockby");
                username = jsonObject.getString("username");
                Log.e("username_user_id", username_user_id);
//                    Utility.setUserName(username);
                Utility.setUserNameId(senderId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String messageBody = remoteMessage.getData().get("body");
            String messagetype = remoteMessage.getData().get("messagetype");
            Log.d(TAG, "messagetype: " + messagetype);

            if (Utility.isChat()) {
                if (!(id + "_" + username).equals(Utility.getselectedUserNameId())) {
                    Log.d(TAG, "noteeee: " + id + "_" + username + "-------" + Utility.getselectedUserNameId());
                    sendNotification(messagetype, messageBody);
                }
            } else sendNotification(messagetype, messageBody);


        }


    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        storeRegIdInPref(token);
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }

    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");

    }

    PendingIntent pendingIntent;

    private void sendNotification(String messageType, String messageBody) {
      //  ChatActivity.getInstance().back();
        if (messageType.equals("chat")) {
//                startActivity(new Intent(this, ChatActivity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//                        .putExtra("firebaseid", firebaseUserId)
////                                    .putExtra("color", pos + "")
//                        .putExtra("color",hexCodeTop)
//                        .putExtra("usernameId",id)
//                        .putExtra("recuid", senderId)
//                        .putExtra("blockid",username_user_id)
//                        .putExtra("blockstatus", blockstatus)
//                        .putExtra("blockby",blockby)
//                        .putExtra("username", username));



//            ChatActivity.getInstance().back();
            Intent intent = new Intent(this, ChatActivity.class);
            intent.putExtra("firebaseUserId", firebaseUserId);
            intent.putExtra("hexCodeTop", hexCodeTop);
            intent.putExtra("senderId", senderId);
            intent.putExtra("id", id);
            intent.putExtra("user_id", user_id);
            intent.putExtra("blockstatus", blockstatus);
            intent.putExtra("blockby", blockby);
            intent.putExtra("username", username);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            Log.e("fghjk", "aayushiiiiiii");
            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

        } else {
            Intent intent = new Intent(this, MainActivity.class);
            Utility.setUserName(username);
            intent.putExtra("name", username);
            intent.putExtra("id", id);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
        }


        channelId = getString(R.string.default_notification_channel_id);
        try {
            URL url = new URL(name);
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        RemoteViews expandedView = new RemoteViews(getPackageName(), R.layout.view_expanded_notification);
        expandedView.setTextViewText(R.id.content_title, getString(R.string.app_name));
        expandedView.setTextViewText(R.id.content_text, messageBody);

        RemoteViews collapsedView = new RemoteViews(getPackageName(), R.layout.view_collapsed_notification);
        collapsedView.setTextViewText(R.id.content_title, getString(R.string.app_name));
        collapsedView.setTextViewText(R.id.content_text, messageBody);
        collapsedView.setTextViewText(R.id.timestamp, DateUtils.formatDateTime(this, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME));
        collapsedView.setImageViewBitmap(R.id.notification_img, image);
        //        collapsedView.setImageViewUri(R.id.notification_img, image1);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setCustomContentView(expandedView)
                        .setCustomBigContentView(collapsedView)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        /// startForeground(ONGOING_NOTIFICATION_ID, notificationBuilder);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(messageBody)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(messageBody))
                    .setCustomContentView(expandedView)
                    .setCustomBigContentView(collapsedView)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            notificationManager.notify(0 /* ID of notification */, builder.build());

        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}