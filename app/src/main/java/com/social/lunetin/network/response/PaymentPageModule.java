package com.social.lunetin.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentPageModule {

    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;
    @Expose
    @SerializedName("data")
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("package2")
        private Package2 package2;
        @Expose
        @SerializedName("package1")
        private Package1 package1;
        @Expose
        @SerializedName("invited")
        private String invited;
        @Expose
        @SerializedName("totalinvite")
        private String totalinvite;
        @Expose
        @SerializedName("watchvideo")
        private String watchvideo;
        @Expose
        @SerializedName("totalvideo")
        private String totalvideo;

        public Package2 getPackage2() {
            return package2;
        }

        public void setPackage2(Package2 package2) {
            this.package2 = package2;
        }

        public Package1 getPackage1() {
            return package1;
        }

        public void setPackage1(Package1 package1) {
            this.package1 = package1;
        }

        public String getInvited() {
            return invited;
        }

        public void setInvited(String invited) {
            this.invited = invited;
        }

        public String getTotalinvite() {
            return totalinvite;
        }

        public void setTotalinvite(String totalinvite) {
            this.totalinvite = totalinvite;
        }

        public String getWatchvideo() {
            return watchvideo;
        }

        public void setWatchvideo(String watchvideo) {
            this.watchvideo = watchvideo;
        }

        public String getTotalvideo() {
            return totalvideo;
        }

        public void setTotalvideo(String totalvideo) {
            this.totalvideo = totalvideo;
        }
    }

    public static class Package2 {
        @Expose
        @SerializedName("packageidandroid")
        private String packageidandroid;
        @Expose
        @SerializedName("packagename")
        private String packagename;
        @Expose
        @SerializedName("packageid")
        private String packageid;
        @Expose
        @SerializedName("amount")
        private String amount;
        @Expose
        @SerializedName("users")
        private String users;

        public String getPackageidandroid() {
            return packageidandroid;
        }

        public void setPackageidandroid(String packageidandroid) {
            this.packageidandroid = packageidandroid;
        }

        public String getPackagename() {
            return packagename;
        }

        public void setPackagename(String packagename) {
            this.packagename = packagename;
        }

        public String getPackageid() {
            return packageid;
        }

        public void setPackageid(String packageid) {
            this.packageid = packageid;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getUsers() {
            return users;
        }

        public void setUsers(String users) {
            this.users = users;
        }
    }

    public static class Package1 {
        @Expose
        @SerializedName("packageidandroid")
        private String packageidandroid;
        @Expose
        @SerializedName("packagename")
        private String packagename;
        @Expose
        @SerializedName("packageid")
        private String packageid;
        @Expose
        @SerializedName("amount")
        private String amount;
        @Expose
        @SerializedName("users")
        private String users;

        public String getPackageidandroid() {
            return packageidandroid;
        }

        public void setPackageidandroid(String packageidandroid) {
            this.packageidandroid = packageidandroid;
        }

        public String getPackagename() {
            return packagename;
        }

        public void setPackagename(String packagename) {
            this.packagename = packagename;
        }

        public String getPackageid() {
            return packageid;
        }

        public void setPackageid(String packageid) {
            this.packageid = packageid;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getUsers() {
            return users;
        }

        public void setUsers(String users) {
            this.users = users;
        }
    }
}
