package com.social.lunetin.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class SignUpModel {

    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;
    @Expose
    @SerializedName("data")
    private Data data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("token")
        private String token;
//        @Expose
//        @SerializedName("usernamelist")
//        private List<String> usernamelist;
        @Expose
        @SerializedName("remail")
        private String remail;
        @Expose
        @SerializedName("total_username_allow")
        private String total_username_allow;
        @Expose
        @SerializedName("refercode")
        private String refercode;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("userId")
        private String userId;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

//        public List<String> getUsernamelist() {
//            return usernamelist;
//        }
//
//        public void setUsernamelist(List<String> usernamelist) {
//            this.usernamelist = usernamelist;
//        }

        public String getRemail() {
            return remail;
        }

        public void setRemail(String remail) {
            this.remail = remail;
        }

        public String getTotal_username_allow() {
            return total_username_allow;
        }

        public void setTotal_username_allow(String total_username_allow) {
            this.total_username_allow = total_username_allow;
        }

        public String getRefercode() {
            return refercode;
        }

        public void setRefercode(String refercode) {
            this.refercode = refercode;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }
}
