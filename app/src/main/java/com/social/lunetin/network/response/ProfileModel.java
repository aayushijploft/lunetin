package com.social.lunetin.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileModel {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;


    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }


    public static class Data {
        @Expose
        @SerializedName("invited")
        private String invited;
        @Expose
        @SerializedName("userdetail")
        private Userdetail userdetail;

        public String getInvited() {
            return invited;
        }

        public void setInvited(String invited) {
            this.invited = invited;
        }

        public Userdetail getUserdetail() {
            return userdetail;
        }

        public void setUserdetail(Userdetail userdetail) {
            this.userdetail = userdetail;
        }
    }

    public static class Userdetail {
        @Expose
        @SerializedName("usernamelist")
        private List<Usernamelist> usernamelist;
        @Expose
        @SerializedName("remail")
        private String remail;
        @Expose
        @SerializedName("referUrl")
        private String referUrl;
        @Expose
        @SerializedName("total_username_allow")
        private String total_username_allow;
        @Expose
        @SerializedName("refercode")
        private String refercode;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("userId")
        private String userId;

        public String getReferUrl() {
            return referUrl;
        }

        public void setReferUrl(String referUrl) {
            this.referUrl = referUrl;
        }

        public List<Usernamelist> getUsernamelist() {
            return usernamelist;
        }

        public void setUsernamelist(List<Usernamelist> usernamelist) {
            this.usernamelist = usernamelist;
        }

        public String getRemail() {
            return remail;
        }

        public void setRemail(String remail) {
            this.remail = remail;
        }

        public String getTotal_username_allow() {
            return total_username_allow;
        }

        public void setTotal_username_allow(String total_username_allow) {
            this.total_username_allow = total_username_allow;
        }

        public String getRefercode() {
            return refercode;
        }

        public void setRefercode(String refercode) {
            this.refercode = refercode;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }

    public static class Usernamelist {
        @Expose
        @SerializedName("messagecount")
        private String messagecount;
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("selected")
        private String selected;
        @Expose
        @SerializedName("hexCodeBottom")
        private String hexCodeBottom;
        @Expose
        @SerializedName("hexCodeTop")
        private String hexCodeTop;
        @Expose
        @SerializedName("firebase_user_id")
        private String firebase_user_id;
        @Expose
        @SerializedName("chatcount")
        private String chatcount;
        @Expose
        @SerializedName("username")
        private String username;
        @Expose
        @SerializedName("user_id")
        private String user_id;
        @Expose
        @SerializedName("id")
        private String id;

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getSelected() {
            return selected;
        }

        public void setSelected(String selected) {
            this.selected = selected;
        }

        public String getFirebase_user_id() {
            return firebase_user_id;
        }

        public void setFirebase_user_id(String firebase_user_id) {
            this.firebase_user_id = firebase_user_id;
        }

        public String getChatcount() {
            return chatcount;
        }

        public void setChatcount(String chatcount) {
            this.chatcount = chatcount;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMessagecount() {
            return messagecount;
        }

        public void setMessagecount(String messagecount) {
            this.messagecount = messagecount;
        }

        public String getHexCodeBottom() {
            return hexCodeBottom;
        }

        public void setHexCodeBottom(String hexCodeBottom) {
            this.hexCodeBottom = hexCodeBottom;
        }

        public String getHexCodeTop() {
            return hexCodeTop;
        }

        public void setHexCodeTop(String hexCodeTop) {
            this.hexCodeTop = hexCodeTop;
        }
    }
}
