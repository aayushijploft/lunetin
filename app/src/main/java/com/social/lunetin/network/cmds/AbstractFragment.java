package com.social.lunetin.network.cmds;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Observer;

public abstract class AbstractFragment extends Fragment implements Observer {
    protected BasicModel model;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        model = getModel();
        if (model != null)
            model.addObserver(this);
        return onCreateViewPost(inflater, container, savedInstanceState);
    }

    protected abstract View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    protected abstract BasicModel getModel();

}