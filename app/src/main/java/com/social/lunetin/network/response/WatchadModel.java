package com.social.lunetin.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WatchadModel {

    @Expose
    @SerializedName("requiread")
    private String requiread;
    @Expose
    @SerializedName("adcount")
    private String adcount;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;

    public String getRequiread() {
        return requiread;
    }

    public void setRequiread(String requiread) {
        this.requiread = requiread;
    }

    public String getAdcount() {
        return adcount;
    }

    public void setAdcount(String adcount) {
        this.adcount = adcount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
