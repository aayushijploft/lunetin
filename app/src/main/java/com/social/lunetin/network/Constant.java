package com.social.lunetin.network;

public class Constant {

    public static final String REGISTER = "api/register";
    public static final String LOGIN = "api/login";
    public static final String SOCIALLOGIN = "api/sociallogin";
    public static final String USERSUGGESTION = "api/usersuggestion";
    public static final String ADDUSERNAME = "api/addusername";
    public static final String USERLIST = "api/userlist";
    public static final String REPORTLIST = "api/reportlist";
    public static final String REPORT = "api/report";
    public static final String MYPROFILE = "api/myprofile";
    public static final String SENDCHAT = "api/chat";
    public static final String FEEDBACk = "api/feedback";
    public static final String PAYMENTPAGE = "api/paymentpage";
    public static final String RESETCHAT = "api/resetchatcount";
    public static final String RATEAPP = "api/rateapp";
    public static final String REPORTUSER = "api/reportperson";
    public static final String BLOCKUSER = "api/blockuser";
    public static final String WATCHAD = "api/watchad";

    public static final Object LOG_FILE_NAME = "staffster.log";
}
