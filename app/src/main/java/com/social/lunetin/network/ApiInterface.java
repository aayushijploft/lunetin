package com.social.lunetin.network;

import com.social.lunetin.network.response.AddUserModel;
import com.social.lunetin.network.response.PaymentPageModule;
import com.social.lunetin.network.response.ProfileModel;
import com.social.lunetin.network.response.ReportModel;
import com.social.lunetin.network.response.SignUpModel;
import com.social.lunetin.network.response.UserListModel;
import com.social.lunetin.network.response.UserSuggestionModel;
import com.social.lunetin.network.response.UsernameModel;
import com.social.lunetin.network.response.WatchadModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST(Constant.REGISTER)
    Call<SignUpModel> register(@FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.LOGIN)
    Call<SignUpModel> login(@FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.SOCIALLOGIN)
    Call<SignUpModel> Sociallogin(@FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.USERSUGGESTION)
    Call<UserSuggestionModel> getUserSuggestions(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.ADDUSERNAME)
    Call<AddUserModel> addUsername(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.USERLIST)
    Call<UserListModel> getUserList(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @GET(Constant.REPORTLIST)
    Call<ReportModel> getReportList();

    @FormUrlEncoded
    @POST(Constant.REPORT)
    Call<UsernameModel> submitReport(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.SENDCHAT)
    Call<UsernameModel> sendChat(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @POST(Constant.MYPROFILE)
    Call<ProfileModel> getProfile(@Header("Authorization") String token);

    @POST(Constant.PAYMENTPAGE)
    Call<PaymentPageModule> getPaymentPage(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(Constant.FEEDBACk)
    Call<UsernameModel> submitFeedback(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.RESETCHAT)
    Call<UsernameModel> resetChat(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.RATEAPP)
    Call<UsernameModel> rateApp(@Header("Authorization") String token,@FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.REPORTUSER)
    Call<UsernameModel> reposrtUser(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.BLOCKUSER)
    Call<UsernameModel> blockUser(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @POST(Constant.WATCHAD)
    Call<WatchadModel> watchAd(@Header("Authorization") String token);
}
