package com.social.lunetin.network.cmds;

import java.util.Observable;

public class BasicModel extends Observable {
    public BasicModel() {
    }

    public void notifyObservers(Object data) {
        setChanged();
        super.notifyObservers(data);
    }
}