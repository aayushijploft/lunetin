package com.social.lunetin.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.social.lunetin.R;
import com.social.lunetin.Utils.ItemClickListener;
import com.social.lunetin.network.response.ReportModel;

import java.util.List;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder> {
    private Context context;
    List<ReportModel.Data> reportList;
    private ItemClickListener itemClickListener;


    public ReportAdapter(Context context,List<ReportModel.Data> reportList,ItemClickListener itemClickListener) {
        this.context = context;
        this.reportList = reportList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ReportAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.report_item_layout, parent, false);
        return new ReportAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ReportAdapter.ViewHolder holder, final int position) {

        holder.tvReportName.setText(reportList.get(position).getName());
        holder.layoutReport.setOnClickListener(view -> itemClickListener.itemClick(position));

    }

    @Override
    public int getItemCount() {
        return reportList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvReportName;
        LinearLayout layoutReport;
        public ViewHolder(View itemView) {
            super(itemView);
            tvReportName = itemView.findViewById(R.id.tvReportName);
            layoutReport = itemView.findViewById(R.id.layoutReport);
        }
    }
}





