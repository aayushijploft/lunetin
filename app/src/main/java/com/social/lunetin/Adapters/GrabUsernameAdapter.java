package com.social.lunetin.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.social.lunetin.R;
import com.social.lunetin.Utils.ItemClickListener;
import com.social.lunetin.network.response.ProfileModel;

import java.util.List;

public class GrabUsernameAdapter extends RecyclerView.Adapter<GrabUsernameAdapter.ViewHolder> {
    private Context context;
    List<ProfileModel.Usernamelist> profileModelList;
    private ItemClickListener itemClickListener;


    public GrabUsernameAdapter(Context context,List<ProfileModel.Usernamelist> profileModelList,ItemClickListener itemClickListener) {
        this.context = context;
        this.profileModelList = profileModelList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public GrabUsernameAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.grab_username, parent, false);
        return new GrabUsernameAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(GrabUsernameAdapter.ViewHolder holder, final int position) {
        String upperString = profileModelList.get(position).getUsername().substring(0, 1).toUpperCase()
                + profileModelList.get(position).getUsername().substring(1).toLowerCase();

        holder.tvUserName.setText(upperString);
        if(profileModelList.get(position).getUsername().equals("Choose a username")){
            holder.tvCount.setVisibility(View.GONE);
        }
        else  {
            if(profileModelList.get(position).getMessagecount().equals("0")){
                holder.tvCount.setVisibility(View.GONE);
            }
            else {
                holder.tvCount.setVisibility(View.VISIBLE);
                holder.tvCount.setText(profileModelList.get(position).getMessagecount());
                Typeface typeface = ResourcesCompat.getFont(context, R.font.opnesans_bold);
                holder.tvUserName.setTypeface(typeface);
            }

        }


        holder.layoutUserlist.setOnClickListener(view -> itemClickListener.itemClick(position));
        holder.tvUserName.setOnClickListener(view -> itemClickListener.itemClick(position));




    }

    @Override
    public int getItemCount() {
        return profileModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCount,tvUserName;
        LinearLayout layoutUserlist;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCount = itemView.findViewById(R.id.tvCount);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            layoutUserlist = itemView.findViewById(R.id.layoutUserlist);


        }
    }
}

