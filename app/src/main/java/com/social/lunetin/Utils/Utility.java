package com.social.lunetin.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.social.lunetin.LetsChat;
import com.social.lunetin.network.response.SignUpModel;

import java.util.regex.Pattern;


public class Utility {
    public static SharedPreferences getPref() {
        return LetsChat.getAppContext().getSharedPreferences("LuneTin", Context.MODE_PRIVATE);
    }

    public static boolean isLogin() {
        return getPref().getBoolean("isalwaysLogin", false);
    }

    public static boolean isRate() {
        return getPref().getBoolean("isRate", false);
    }
    public static boolean isChat() {
        return getPref().getBoolean("isChat", false);
    }

    public static void saveNumber( String number) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("number", number);
        // editor.putBoolean("isalwaysLogin", true);
        editor.apply();
    }

    public static void saveUserID( String number) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("user_id", number);
        // editor.putBoolean("isalwaysLogin", true);
        editor.apply();
    }
    public static void saveNotification( String number) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("Notification", number);
        // editor.putBoolean("isalwaysLogin", true);
        editor.apply();
    }

    public static void setFriend( String lat) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("friendID", String.valueOf(lat));
        editor.apply();
    }

    public static String getFriend() {
        return getPref().getString("friendID", "");
    }
    public static String getNotification() {
        return getPref().getString("Notification", "0");
    }

    public static String getlng() {
        return getPref().getString("lng", "0.0");
    }

    public static String getpic() {
        return getPref().getString("url", "");
    }


    public static void logout() {
        getPref().edit().clear().apply();
    }

    public static String getLoginNumber() {
        return getPref().getString("number", "xxxxxxxxxx");
    }

    public static String getLoginId() {
        return getPref().getString("user_id", "");
    }

    public static String getagent_id() {
        return getPref().getString("agent_id", "");
    }

    public static String getLoginFullName() {
        return getPref().getString("name", "Edso");
    }


    public static String getLoginEmail() {
        return getPref().getString("email", "");
    }


    public static String getdob() {
        return getPref().getString("dob", "");
    }


    public static String getLoginPhone() {
        return getPref().getString("number", "");
    }

    public static String getIPaddress() {
        return getPref().getString("IP", "");
    }

    public static String getGenter() {
        return getPref().getString("gender", "");
    }

    public static String getreligion() {
        return getPref().getString("religion", "");
    }

    public static String getEmail() {
        return getPref().getString("email", "");
    }

    public static String getToken() {
        return getPref().getString("token", "");
    }

    public static String getPassword() {
        return getPref().getString("password", "");
    }

    public static void setdetail( String email, String password) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("email", email);
        editor.putString("password", password);
        editor.apply();

    }

    public static String email() {
        return getPref().getString("email", "");
    }


  public static void setDashBoardData( SignUpModel.Data data) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("email", data.getEmail());
        editor.putString("token", data.getToken());
        editor.putString("total_username_allow", data.getTotal_username_allow());
        editor.putString("name", data.getName());
        editor.putString("remail", data.getRemail());
        editor.putString("refercode", data.getRefercode());
        editor.putString("userId", data.getUserId());
        editor.apply();

    }


    public static void setlogin() {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putBoolean("isalwaysLogin", true);
        editor.apply();

    }
    public static void setRate() {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putBoolean("isRate", true);
        editor.apply();

    }
    public static void setChat(boolean v) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putBoolean("isChat", v);
        editor.apply();

    }

    public static void setloginID( String id) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("user_id", id);
        editor.apply();

    }
    public static void setFirbaseID( String id) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("firbase_user_id", id);
        editor.apply();

    }
    public static void setIPAddresss( String id) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("IP", id);
        editor.apply();

    }

    public static void setUserName( String fName) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("username", fName);
        editor.apply();

    }
    public static void setUserNameId( String fName) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("usernameid", fName);
        editor.apply();

    }
    public static void setselectedUserNameId( String fName) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("selected_id", fName);
        editor.apply();

    }

    public static String getselectedUserNameId() {
        return getPref().getString("selected_id", "");
    }


    public static void setProfilePic( String image) {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("profile_image", image);
        editor.apply();

    }


    public static String getUserName() {
        return getPref().getString("username", "");
    }
    public static String getUserNameId() {
        return getPref().getString("usernameid", "");
    }

    public static String getFirbaseID() {
        return getPref().getString("firbase_user_id", "");
    }
    public static String getUserEmail() {
        return getPref().getString("Email", "");
    }

    public static String getProfilePic() {
        return getPref().getString("profile_image", "");
    }


    public static void setlogout() {

        SharedPreferences.Editor editor = getPref().edit();
        editor.putBoolean("isalwaysLogin", false);
        editor.clear();
        editor.apply();

    }


    public static boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    static public void setVibes( Object modal) {
        /**** storing object in preferences  ****/
        SharedPreferences.Editor editor = getPref().edit();
        Gson gson = new Gson();
        String jsonObject = gson.toJson(modal);
        editor.putString("vibes", jsonObject);
        editor.commit();

    }

    public static void setGender(String gender) {
        SharedPreferences.Editor editor = getPref().edit();
        editor.putString("gender", gender);
        editor.apply();
    }

    public static String getGender() {
        return getPref().getString("gender", "male");
    }

}
